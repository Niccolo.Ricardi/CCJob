#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sat Sep 15 18:34:00 2018

@author: Alexander Zech
"""
info = {"author" : "Nico Ricardi",
        "acknowledgment": "Alex Zech and Cristina Gonzalez",
        "version": "0.1.0",
        "date"   : "November 19th, 2018"}

import CCJob as ccj
import CCParser as ccp
import os
import numpy as np
import shutil as sh
import glob as gl
import re
import CCJob.utils as ut
import subprocess as sp
from CCJob.Composable_templates import Tdefaults, Tinp, Trem_kw,\
Tfragments, Tfde
import logging

Tdflt_rem = Tdefaults["rem_kw"].copy()
Tdflt_rem["fde"] = "true"


# set up logger. NB avoid homonimity with other module's loggers (e.g. ccp)
ccjlog = logging.getLogger("ccj")
#ut.setupLogger()

class NotConvergedError(Exception):
    pass

def print_iteration(it, lw=80, opt="FT"):
    dict_={"FT":"Freeze-and-Thaw", "MC": "Conventional FDET"}
    if opt not in dict_.keys():
        dict_ [opt]="Unknown kind of cycle"
    sw = 20
    q,m = divmod(lw-sw,2)
#    print("*"*lw)
#    print(" "*q, "{0} #{1}".format(dict_[opt],it))
#    print("*"*lw)
    ccjlog.critical("*"*lw)
    ccjlog.critical(" "*q + "{0} #{1}".format(dict_[opt],it))
    ccjlog.critical("*"*lw)
    
def print_banner(string, lw=80):
    sw = len(string)
    q,m = divmod(lw-sw,2)
#    print("*"*lw)
#    print(" "*q, string)
#    print("*"*lw)
    ccjlog.critical("*"*lw)
    ccjlog.critical(" "*q + string)
    ccjlog.critical("*"*lw)
        
def check_first_line(fname):
    """
    Parameters
    ----------
    fname: str
        the filename to check
        
    Returns
    -------
    tuple(bool, int, bool)
        whether the file has a header, the number of basis, whether it has only alpha
    """
    first_line_splt = str(sp.check_output(["head", "-n 1", fname]))[2:].split()
    header = True if len(first_line_splt) > 1 else False
    if header:
        Nbas = int(first_line_splt[1])
    nl = int(str(sp.check_output(["wc", "-l", fname]))[2:].split()[0])
    nl -= 1 if header else 0
#    nl += nl % 2  # sometimes wc -l returns one less than lines, we fix it here if necessary
    alpha_only = True if ut.is_square(nl) else False
    nl /= 1 if alpha_only else 2
    Nbas = int(nl ** 0.5)
    return header, Nbas, alpha_only

def get_naos(fname, header=None, alpha_only=None):
    """
    Parameters
    ----------
    fname: str
        the filename to check
    header: bool/None
        whether the file has a header
    alpha_only: bool/None
        whether the file has only alpha matrix or alpha, beta
        
    Returns
    -------
    int
        the number of basis/aos
    """
    first = False
    if header == None:
        first = str(sp.check_output(["head", "-n 1", fname])).split()
        header = True if len(first) == 3 else False
    if header:
        if first:
            return int(first[1])
        else:
            return int(str(sp.check_output(["head", "-n 1", fname])).split()[1])
    else:
        nl = int(str(sp.check_output(["wc", "-l", fname]))[2:].split()[0])
#        nl += nl % 2  # sometimes wc -l returns one less than lines, we fix it here if necessary
        if alpha_only == None:
            alpha_only = alpha_only = True if ut.is_square(nl) else False
        nl /= 1 if alpha_only else 2
        Nbas = nl ** 0.5
    return int(Nbas)
    
def read_density(fname, header=None, alpha_only=None):
    """
    Parameters
    ----------
    fname: str
        the DM filename
    head: bool/None
        whether the file has a header, with None(default) that is deduced
    alpha_only: bool/None
        whether the file has only one matrix(alpha), with None(default) that is deduced
    
    Return
    ------
    np.array(2, Nbas, Nbas)
        alpha and beta DM
    """
    if header == None or alpha_only == None:
        header, Nbas, alpha_only = check_first_line(fname)
    else:
        Nbas = get_naos(fname, header=header, alpha_only=alpha_only)
    D = np.loadtxt(fname, dtype=np.float64, skiprows=1 if header else 0)
    D = np.array([D.reshape((Nbas, Nbas)),D.reshape((Nbas, Nbas))]) if alpha_only \
    else D.reshape((2, Nbas, Nbas))
    return D

def transform_DM(matrix, order):
    """Reorder an AO matrix given a certain order.
    
    Parameters
    ----------
    matrix : np.ndarray
        Input matrix in AO basis.
    order : list
        List of indices in new order.
        ``[0,1,2,...,n]`` will yield the same matrix again.
    
    Returns
    -------
    X : np.ndarray
        Reordered matrix.
    """
    shape = matrix.shape
    if len(shape) == 3:
        to_return = np.zeros(shape)
        for i in range(2):
            to_return[i] = transform_DM(matrix[i], order=order)
        return to_return
    else:
        nAO = matrix.shape[0]
        I = np.identity(nAO)
        P = I[:, order]
        X = np.dot(np.dot(P.T, matrix), P)
    return X

def write_density(filename, D, alpha_only=False, header=True):
    """Save density matrix tensor using numpy.
    
    Parameters
    ----------
    filename : str
        Output file name.
    X : np.ndarray
        (Density) matrix
    alpha_only : bool
        Whether only the alpha density matrix should be written to file.
    header : bool
        Whether to print a header to file or not.
    """
    h = "" if not header else " ".join([str(i) for i in D.shape])
    with open(filename, "w") as f:
        if alpha_only:
            np.savetxt(f, D, delimiter="\n", fmt="%14.10e", header=h, comments='')
        else:
            for n,slice_2d in enumerate(D):
                np.savetxt(f, slice_2d, delimiter="\n", fmt="%14.10e", comments='',
                           header=h if n == 0 else "")
    
def copy_density(src, dst, header_src=None, alpha_only_src=None, header_dst=True, alpha_only_dst=False):
    """
    Parameters
    ----------
    src: str
        source(filename/path)
    dst: str
        destinationfilename/path)
    header_src: bool/None
        whether the source file has a header, with None(default) that is deduced
    alpha_only_src: bool/None
        whether the source file has only one matrix(alpha), with None(default) that is deduced
    header_dst: bool
        whether the destination file should have a header. Default is True
    alpha_only_dst: bool
        whether the destination file should have only one matrix(alpha). Default is False
    """
    Nbas = False
    if header_src == None or alpha_only_src == None:
        header_src, Nbas, alpha_only_src = check_first_line(src)
        print(header_src, Nbas, alpha_only_src)
    else:
        Nbas = get_naos(src, header=header_src, alpha_only=alpha_only_src)
    skip = [0, int(header_dst)]
    if os.path.isfile(dst):
        os.remove(dst)
    if not header_src and header_dst:
        sp.call("echo \"2 {Nbas} {Nbas}\" > {dst}".format(Nbas=Nbas, dst=dst), shell=True)
    if header_src and not header_dst:
        skip[0] = 1
    if alpha_only_src:
        for n in range(1 if alpha_only_dst else 2):
            sp.call("tail -n +{offset} {src} >> {dst}".format(offset=skip[n]+1, src=src, dst=dst), shell=True) 
    else:
        if alpha_only_dst:
            sp.call("sed -n {start},{end}p {src} >> {dst}".format(start=skip[0], end=Nbas**2+skip[0], src=src, dst=dst), shell=True)
        else:
            sp.call("tail -n +{offset} {src} >> {dst}".format(offset=skip[0]+1, src=src, dst=dst), shell=True) 

def get_embedded_energy(outfile, with_ccp=True, json_file="", log_file=""):
    """ Get embedded energy of subsystem A.

    Parameters
    ----------
    outfile : :obj:`str`
        Filename of output to be parsed
    with_ccp : :obj:`bool`
        Whether or not to use CCParser
    """
    E_A = []
    if with_ccp:
        # Parse output
        kwargs = dict(to_console=False, to_file=True, to_json=True)
        if json_file:
            kwargs.update(dict(json_file=json_file))
        if log_file:
            kwargs.update(dict(log_file=log_file))
        out = ccp.Parser(outfile, **kwargs)
        E_A = out.results.E_A.data
    else:
        with open(outfile) as f:
            for i, line in enumerate(f):
                if "Embedded System (A):" in line:
                    E_A.append(float(line.split()[-2]))

    # the order is generally HF(0), post-HF(1->nstates)
    if len(E_A) <= 2:
        return E_A[-1]
    elif len(E_A) > 2:
        # if for some reason we also calculate excited states
        return E_A[1]
    
def get_nbas(filepath, with_ccp=True, json_file="", log_file="", index=[2,3,4]): 
    """ Get number of basis functions"""
    nbas = {"nbasA" : 0, "nbasB": 0, "nbasAB": 0}
    default_ccpjson = "CCParser.json"
    dir_, file = os.path.split(filepath)[0], os.path.split(filepath)[1]
    if with_ccp:
        if filepath[-4:] == "json":
            json_file = filepath
        elif os.path.isfile(os.path.join(dir_, json_file)):
            json_file=os.path.join(dir_, json_file)
        else:
            json_files = gl.glob(os.path.join(dir_,"*.json"))
            kwargs = dict(to_console=False, to_file=True, to_json=True)
            if json_file:
                kwargs.update(dict(json_file=json_file))
            if log_file:
                kwargs.update(dict(log_file=log_file))
            out = ccp.Parser(filepath, **kwargs)
            try:
                json_file = [i for i in gl.glob(os.path.join(dir_,"*.json")) if i not in json_files][0]
                assert json_file == default_ccpjson
            except IndexError:
                ccjlog.critical("The json file was already there. Will use default name: {}".format(default_ccpjson))
            except AssertionError:
                ccjlog.critical("CCParser's default seems to be \"{}\" change default_ccpjson in this function!!".format(json_file))
                default_ccpjson = json_file
                
        parsed = ut.load_js(json_file)
        nbas["nbasAB"] = parsed["nbas"][index[0]][0]
        nbas["nbasA"]  = parsed["nbas"][index[1]][0]
        nbas["nbasB"]  = parsed["nbas"][index[2]][0]
    else:
        with open(filepath) as f:
            r = f.read()
        pattern = r"There are (?P<shells>\d+) shells and (?P<bsf>\d+) basis functions"
        parsed  = re.findall(pattern, r)
        nbas["nbasAB"] = parsed[index[0]][1]
        nbas["nbasA"]  = parsed[index[1]][1]
        nbas["nbasB"]  = parsed[index[2]][1]
    return nbas    

def macrocycles(queue, rem_kw={}, fde_kw={}, extras="", maxiter=8, thresh=1e-8,
                use_zr=True, fragments={}, elconf={}, basename="MC_{}",
                with_ccp=True, lw=80, q_custom=None, en_file="energies.txt"):
    # General
    iterDirName = "cy"
    # Important paths
    root = os.getcwd()
    en_file = os.path.join(root, en_file)
    
    # XYZ structures (default: from .zr file)
    if use_zr:
#        print("-- XYZ from ZR file")
        ccjlog.critical("-- XYZ from ZR file")
        zr_file = ccj.find_file(root, extension="zr")
        frags = ccj.zr_frag(zr_file)
    else:
#        print("-- XYZ from fragment input")
        print("-- XYZ from fragment input")
        frags = fragments
#    print("-- Number of atoms (A): ", len(frags["A"].split("\n")))
#    print("-- Number of atoms (B): ", len(frags["B"].split("\n")))
    ccjlog.critical("-- Number of atoms (A): {}".format(len(frags["A"].split("\n"))))
    ccjlog.critical("-- Number of atoms (B): {}".format(len(frags["B"].split("\n"))))

    # try to get electronic configuration if needed
    if not elconf:
        f_elconf = ccj.find_eleconfig(root)
        elconf= ccj.read_eleconfig(fname=f_elconf)
#    print("-- Charge (A)      : ", elconf["charge_a"])
#    print("-- Multiplicity (A): ", elconf["multiplicity_a"])
#    print("-- Charge (B)      : ", elconf["charge_b"])
#    print("-- Multiplicity (B): ", elconf["multiplicity_b"])
    ccjlog.critical("-- Charge (A)      : {}".format(elconf["charge_a"]))
    ccjlog.critical("-- Multiplicity (A): {}".format(elconf["multiplicity_a"]))
    ccjlog.critical("-- Charge (B)      : {}".format(elconf["charge_b"]))
    ccjlog.critical("-- Multiplicity (B): {}".format(elconf["multiplicity_b"]))

    # Iteration related
    if os.path.isfile(en_file):
        energy = np.loadtxt(en_file).reshape(-1).tolist()  # to avoid issues if 1 value only
        it = len(energy)
#        print("Restarting Conventional FDET from iteration {}".format(it))
        ccjlog.critical("Restarting Conventional FDET from iteration {}".format(it))
    else:
        it = 0
        energy = []
    itname = basename.format(max(it - 1, 0)) if "{}" in basename else basename
    path_cur = os.path.join(root, iterDirName + str(max(it - 1, 0)))
    # Create and change into first folder
    ut.mkdif(path_cur)
    ut.logchdir(ccjlog,path_cur)
    
    # A_init, B_init are True if the initial DM is given
    A_init = bool(re.search("import_?rho_?a", fde_kw, re.IGNORECASE)) if type(fde_kw) == str \
    else "import" in fde_kw["method_a"]  # allows for variations (upper/lower case, = or not)
    B_init = bool(re.search("import_?rho_?b", fde_kw, re.IGNORECASE)) if type(fde_kw) == str \
    else "import" in fde_kw["method_a"] 
    s_rem_kw = rem_kw if type(rem_kw) == str else \
    Trem_kw.substitute(Tdflt_rem, **rem_kw)
    if type(fde_kw) == str:
        if A_init and B_init:
            s_fde = fde_kw
        else:
            raise ValueError("fde_kw as string only works with initial rhoA and rhoB! I can't edit the string!")
    else:
        s_fde = Tfde.substitute(Tdefaults["fde"], **fde_kw)
    if extras:
        if type(extras) == list:
            extras = "\n".join(extras)
    extra_sects = "\n".join([extras, s_fde])
    frag_specs = dict(frag_a=frags["A"], frag_b=frags["B"])
    frag_specs.update(elconf)
    s_frags = Tfragments.substitute(ut.myupd(Tdefaults["molecule"], frag_specs))
    specs = ut.myupd(Tdefaults["inp"], rem_kw=s_rem_kw, molecule=s_frags, extras=extra_sects)
    if it == 0:
        if A_init:
            sh.copy(os.path.join(root, "Densmat_A.txt"), path_cur)
        if B_init:
            path_B = os.path.join(root, "Densmat_B.txt")
        sh.copy(path_B, path_cur)
        ### FIRST JOB
        inp = ccj.Input.from_template(Tinp, itname+".in", **specs)
        queue["jobname"] = itname
        job = ccj.Job(inp, **queue)
        if q_custom != None:
            job.set_custom_options(**q_custom)
        
        # Run the first job
        print_iteration(it, opt="MC")
        job.run()
        
    #    # Parse output
    #    out = ccp.Parser(itname+".out", software="qchem",
    #                     to_console=False, to_file=True, to_json=True,
    #                     log_file="parser_{0}.log".format(it),
    #                     json_file="parser_{0}.json".format(it))
    #    
    #    # get embedded energy of active system
    #    # the order is generally HF(0), post-HF(1->nstates)
    #    if len(out.results.E_A) <= 2:
    #        energy.append(out.results.E_A.get_last())
    #    elif len(out.results.E_A) > 2:
    #        # if for some reason we also calculate excited states
    #        energy.append(out.results.E_A[1])
        E_A = get_embedded_energy(itname+".out", with_ccp=with_ccp,
                                          json_file="{}.json".format(itname),
                                          log_file="{}.log".format(itname))
        sp.call("echo {E_A} >> {en_file}".format(E_A=E_A, en_file=en_file), shell=True)
        energy.append(E_A)
        
        # adjust iteration counter
        it += 1
    
    if not A_init:
        fde_kw["method_a"] = "import_rhoA true"
    if not B_init:
        fde_kw["method_b"] = "import_rhoB true"
        expansion = fde_kw["expansion"] if "expansion" in fde_kw.keys() else "ME"
        densMat_B = gl.glob("frag1_*_dens_{0}.txt".format(expansion))[0] 
        path_B = os.path.join(root, "Densmat_B.txt") if not os.path.isfile(os.path.join(root, "Densmat_B.txt"))\
        else os.path.join(root, "Densmat_B_fromMC0.txt")
        copy_density(densMat_B, path_B, header_src=True, alpha_only_src=True)
    else:
        path_B = os.path.join(root, "Densmat_B.txt")
    if not A_init or not B_init:
        s_fde = Tfde.substitute(Tdefaults["fde"], **fde_kw)
        extra_sects = "\n".join([extras, s_fde])
        specs = ut.myupd(Tdefaults["inp"], rem_kw=s_rem_kw, molecule=s_frags, extras=extra_sects)
    difference_to_last = energy[-1] - energy[-2] if it >= 2 else 1
    #------------------------------------------------------------------------------
    #         ALL OTHER JOBS
    #------------------------------------------------------------------------------
    while abs(difference_to_last) > thresh and it < maxiter:
        itname = basename.format(it) if "{}" in basename else basename
        print_iteration(it, opt="MC")
        # Folders
        path_old = path_cur
        path_cur = os.path.join(root, iterDirName + str(it))
        ut.mkdif(path_cur)
        ut.logchdir(ccjlog,path_cur)
        
        # copy density matrix/-ices from last iteration
        sh.copy(path_B, "Densmat_B.txt")
        copy_density(os.path.join(path_old, "FDE_State0_tot_dens.txt"), "Densmat_A.txt",
                     header_src=False, alpha_only_src=False)
        # create input object
        inp = ccj.Input.from_template(Tinp, itname+".in", **specs)
        
        # Prepare job  (like above)
        queue["jobname"] = itname 
        job = ccj.Job(inp, **queue)
        if q_custom != None:
            job.set_custom_options(**q_custom)
    
        # Run the iterative job
        job.run()
        
        # Parse output
#        out = ccp.Parser(itname+".out", software="qchem",
#                         to_console=False, to_file=True, to_json=True,
#                         log_file="parser_{0}.log".format(it),
#                         json_file="parser_{0}.json".format(it))
#        
#        # get embedded energy (see above)
#        if len(out.results.E_A) <= 2:
#            energy.append(out.results.E_A.get_last())
#        elif len(out.results.E_A) > 2:
#            energy.append(out.results.E_A[1])
        E_A = get_embedded_energy(itname+".out", with_ccp=with_ccp,
                                      json_file="{}.json".format(itname),
                                      log_file="{}.log".format(itname))
        sp.call("echo {E_A} >> {en_file}".format(E_A=E_A, en_file=en_file), shell=True)
        energy.append(E_A)
        difference_to_last = energy[it] - energy[it-1]
#        print("-- Current residual: {0: .8e}".format(difference_to_last))
        ccjlog.critical("-- Current residual: {0: .8e}".format(difference_to_last))
        
        # increase iteration counter
        it += 1
        # END WHILE LOOP
    ut.logchdir(ccjlog,root)
#    print("*"*lw)
#    print("[!] Finished FDET macrocycles calculation successfully in {0} iterations.".format(it))
    ccjlog.critical("*"*lw)
    ccjlog.critical("[!] Finished FDET macrocycles calculation successfully in {0} iterations.".format(it))


def freeze_and_thaw(queue, rem_kw={}, fde_kw={}, extras="", maxiter=8, thresh=1e-8,
                    use_zr=True, fragments={}, elconf={}, basename="FT_{}",
                    with_ccp=True, lw=80, q_custom=None, en_file="energies.txt"):
    
    # General
    iterDirName = "cy"
    root = os.getcwd()
    en_file = os.path.join(root, en_file)
    # XYZ structures (default: from .zr file)
    if use_zr:
#        print("-- XYZ from ZR file")
        ccjlog.critical("-- XYZ from ZR file")
        zr_file = ccj.find_file(root, extension="zr")
        frags = ccj.zr_frag(zr_file)
    else:
        ccjlog.critical("-- XYZ from fragment input")
#        print("-- XYZ from fragment input")
        frags = fragments
#    print("-- Number of atoms (A): ", len(frags["A"].split("\n")))
#    print("-- Number of atoms (B): ", len(frags["B"].split("\n")))
    ccjlog.critical("-- Number of atoms (A): {}".format(len(frags["A"].split("\n"))))
    ccjlog.critical("-- Number of atoms (B): {}".format(len(frags["B"].split("\n"))))

    # try to get electronic configuration if needed
    if not elconf:
        f_elconf = ccj.find_eleconfig(root)
        elconf= ccj.read_eleconfig(fname=f_elconf)
    elconf_rev = dict(**elconf)
    elconf_rev["charge_a"] = elconf["charge_b"]
    elconf_rev["charge_b"] = elconf["charge_a"]
    elconf_rev["multiplicity_a"] = elconf["multiplicity_b"]
    elconf_rev["multiplicity_b"] = elconf["multiplicity_a"]
    
#    print("-- Charge (A)      : ", elconf["charge_a"])
#    print("-- Multiplicity (A): ", elconf["multiplicity_a"])
#    print("-- Charge (B)      : ", elconf["charge_b"])
#    print("-- Multiplicity (B): ", elconf["multiplicity_b"])
    ccjlog.critical("-- Charge (A)      : {}".format(elconf["charge_a"]))
    ccjlog.critical("-- Multiplicity (A): {}".format(elconf["multiplicity_a"]))
    ccjlog.critical("-- Charge (B)      : {}".format(elconf["charge_b"]))
    ccjlog.critical("-- Multiplicity (B): {}".format(elconf["multiplicity_b"]))
    
    # Iteration related
    if os.path.isfile(en_file):
        energy = np.loadtxt(en_file).reshape(-1).tolist()  # to avoid issues if 1 value only
        it = len(energy)
#        print("Restarting Freeze-and-Thaw from iteration {}".format(it))
        ccjlog.critical("Restarting Freeze-and-Thaw from iteration {}".format(it))
    else:
        it = 0
        energy = []
    itname = basename.format(max(it - 1, 0)) if "{}" in basename else basename
    path_cur = os.path.join(root, iterDirName + str(max(it - 1, 0)))
    ut.mkdif(path_cur)

    # A_init, B_init are True if the initial DM is given
    A_init = bool(re.search("import_?rho_?[aA]", fde_kw, re.IGNORECASE)) if type(fde_kw) == str \
    else "import" in fde_kw["method_a"]  # allows for variations (upper/lower case, = or not)
    B_init = bool(re.search("import_?rho_?[bB]", fde_kw, re.IGNORECASE)) if type(fde_kw) == str \
    else "import" in fde_kw["method_a"] 
    s_rem_kw = rem_kw if type(rem_kw) == str else \
    Trem_kw.substitute(Tdflt_rem, **rem_kw)
    if type(fde_kw) == str:
        if A_init and B_init:
            try:
                expansion = re.search("expansion\s*=?\s*(?P<exp>..)", fde_kw, re.IGNORECASE)["exp"].upper()
            except TypeError:
                expansion = "ME"  # Expansion not specified, it is qchem's default 
            s_fde = fde_kw
        else:
            raise ValueError("fde_kw as string only works with initial rhoA and rhoB! I can't edit the string!")
    else:
        expansion = fde_kw["expansion"] if "expansion" in fde_kw.keys() else "ME"
        s_fde = Tfde.substitute(Tdefaults["fde"], **fde_kw)
    if extras:
        if type(extras) == list:
            extras = "\n".join(extras)
    extra_sects = "\n".join([extras, s_fde])
    frag_specs = dict(frag_a=frags["A"], frag_b=frags["B"])
    frag_specs.update(elconf)
    frag_specs_rev = dict(frag_a=frags["B"], frag_b=frags["A"])
    frag_specs_rev.update(elconf_rev)
    s_frags = Tfragments.substitute(ut.myupd(Tdefaults["molecule"], frag_specs))
    s_frags_rev = Tfragments.substitute(ut.myupd(Tdefaults["molecule"], frag_specs_rev))
    specs = ut.myupd(Tdefaults["inp"], rem_kw=s_rem_kw, molecule=s_frags, extras=extra_sects)
    specs_rev = ut.myupd(Tdefaults["inp"], rem_kw=s_rem_kw, molecule=s_frags_rev, extras=extra_sects)
    
    if it == 0:
        ### FIRST JOB
        if A_init:
            sh.copy(os.path.join(root, "Densmat_A.txt"), path_cur)
        if B_init:
            path_B = os.path.join(root, "Densmat_B.txt")
            sh.copy(path_B, path_cur)
        # Change into first folder
        ut.logchdir(ccjlog,path_cur)
        inp = ccj.Input.from_template(Tinp, itname+".in", **specs)
        queue["jobname"] = itname
        job = ccj.Job(inp, **queue)
        if q_custom != None:
            job.set_custom_options(**q_custom)
        
        # Run the first job
        print_iteration(it, opt="FT")
        job.run()
        
    #    # Parse output
    #    out = ccp.Parser(itname+".out", software="qchem",
    #                     to_console=False, to_file=True, to_json=True,
    #                     log_file="parser_{0}.log".format(it),
    #                     json_file="parser_{0}.json".format(it))
    #    
    #    # get embedded energy of active system
    #    # the order is generally HF(0), post-HF(1->nstates)
    #    if len(out.results.E_A) <= 2:
    #        energy.append(out.results.E_A.get_last())
    #    elif len(out.results.E_A) > 2:
    #        # if for some reason we also calculate excited states
    #        energy.append(out.results.E_A[1])
        
        E_A = get_embedded_energy(itname+".out", with_ccp=with_ccp,
                                          json_file="{}.json".format(itname),
                                          log_file="{}.log".format(itname))
        sp.call("echo {E_A} >> {en_file}".format(E_A=E_A, en_file=en_file), shell=True)
        energy.append(E_A)
        # adjust iteration counter
        it += 1
    
    last_outputA = os.path.join(root, iterDirName + str(it - 1 - (it - 1) % 2),
                           "{}.out".format(basename.format(it - 1 - (it - 1) % 2) if \
                            "{}" in basename else basename))
    # get number of basis functions from the basis initializer block (constructor)
    #for ME the number of basis is not used. could be moved into "if SE" , kept as check
    nbas = get_nbas(last_outputA, json_file="{}.json".format(itname), with_ccp=with_ccp)  
    nbasAB = nbas["nbasAB"]
    nbasA  = nbas["nbasA"]
    nbasB  = nbas["nbasB"]
#    print("""-- Found number of basis functions:
#       .nbasAB = {0}
#       .nbasA  = {1}
#       .nbasB  = {2}
#    """.format(nbasAB, nbasA, nbasB))
    ccjlog.critical("""-- Found number of basis functions:
       .nbasAB = {0}
       .nbasA  = {1}
       .nbasB  = {2}
    """.format(nbasAB, nbasA, nbasB))
    
    
    # transform each nAO x nAO matrix if supermolecular expansion
    if expansion == "SE":
        number_line = [i for i in range(nbasAB)]
        order_to_B  = number_line[nbasA:] + number_line[:nbasA]
        order_to_A  = number_line[nbasB:] + number_line[:nbasB]

    if not A_init:
        fde_kw["method_a"] = "import_rhoA true"
    if not B_init:
        fde_kw["method_b"] = "import_rhoB true"
        densMat_B = gl.glob("frag1_*_dens_{0}.txt".format(expansion))[0]
        copy_density(densMat_B, "Densmat_B.txt", header_src=True, alpha_only_src=True)
    if not A_init or not B_init:
        s_fde = Tfde.substitute(Tdefaults["fde"], **fde_kw)
        extra_sects = "\n".join([extras, s_fde])
        specs = ut.myupd(Tdefaults["inp"], rem_kw=s_rem_kw, molecule=s_frags, extras=extra_sects)
        specs_rev = ut.myupd(Tdefaults["inp"], rem_kw=s_rem_kw, molecule=s_frags_rev, extras=extra_sects)
        
    is_A = bool((it) % 2)  # different because we haven't run
    difference_to_last = 1 if it < 3 else energy[it-1] - energy[it-3] if is_A else energy[it-2] - energy[it-4]  # -1 is because we haven't run
    #------------------------------------------------------------------------------
    #         ALL OTHER JOBS
    #------------------------------------------------------------------------------ 
    while abs(difference_to_last) > thresh and it < maxiter:
        itname = basename.format(it) if "{}" in basename else basename
        is_A = bool((it+1) % 2)
        print_iteration(it, opt="FT")
        # Folders
        path_old = path_cur
        path_cur = os.path.join(root, iterDirName + str(it))
        ut.mkdif(path_cur)
        ut.logchdir(ccjlog,path_cur)
        
        # copy density matrix/-ices from last iteration
        if expansion == "ME":
            sh.copy(os.path.join(path_old, "Densmat_B.txt"), "Densmat_A.txt")
            copy_density(os.path.join(path_old, "FDE_State0_tot_dens.txt"), "Densmat_B.txt",
                     header_src=False, alpha_only_src=False)
        else:
            # active and frozen refer to current iteration
            active = read_density(os.path.join(path_old, "Densmat_B.txt"), header=True, alpha_only=False)
            frozen = read_density(os.path.join(path_old, "FDE_State0_tot_dens.txt"), header=False, alpha_only=False)
            active = transform_DM(active, order_to_A if is_A else order_to_B)
            frozen = transform_DM(frozen, order_to_A if is_A else order_to_B)
            write_density("Densmat_A.txt", active)
            write_density("Densmat_B.txt", frozen)
        # create input object
        inp = ccj.Input.from_template(Tinp, itname+".in", **specs if is_A else specs_rev)
        
        # Prepare job  (like above)
        queue["jobname"] = itname
        job = ccj.Job(inp, **queue)
        if q_custom != None:
            job.set_custom_options(**q_custom)
    
        # Run the iterative job
        job.run()
        
        # Parse output
#        out = ccp.Parser(itname+".out", software="qchem",
#                         to_console=False, to_file=True, to_json=True,
#                         log_file="parser_{0}.log".format(it),
#                         json_file="parser_{0}.json".format(it))
#        
#        # get embedded energy (see above)
#        if len(out.results.E_A) <= 2:
#            energy.append(out.results.E_A.get_last())
#        elif len(out.results.E_A) > 2:
#            energy.append(out.results.E_A[1])
        E_A = get_embedded_energy(itname+".out", with_ccp=with_ccp,
                                      json_file="{}.json".format(itname),
                                      log_file="{}.log".format(itname))
        sp.call("echo {E_A} >> {en_file}".format(E_A=E_A, en_file=en_file), shell=True)
        energy.append(E_A)
        if is_A:
            difference_to_last = energy[it] - energy[it-2]
#            print("-- Current residual: {0: .8e}".format(difference_to_last))
            ccjlog.critical("-- Current residual: {0: .8e}".format(difference_to_last))
        
        # increase iteration counter
        it += 1
        # END WHILE LOOP
    ut.logchdir(ccjlog,root)
    if it >= maxiter:
        raise NotConvergedError("Freeze-and-Thaw cycles did not converge in "
                                "{0} iterations!".format(maxiter))
    else:
#        print("*"*lw)
#        print(("[!] Finished Freeze-and-Thaw calculation successfully in "
#               "{0} iterations.".format(it)))
        ccjlog.critical("*"*lw)
        ccjlog.critical(("[!] Finished Freeze-and-Thaw calculation successfully in "
               "{0} iterations.".format(it)))