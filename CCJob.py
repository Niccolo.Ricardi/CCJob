#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Aug  4 17:56:34 2021

@author: nico
"""
info = {"author" : "Nico Ricardi",
        "acknowledgment": "Alex Zech and Cristina Gonzalez",
        "version": "2.0.1",
        "date"   : "04 August, 2021"}

import string
import os
import subprocess as sp
import shutil as sh
import glob
from .Templates import defaults
import logging

# set up logger. NB avoid homonimity with other module's loggers (e.g. ccp)
ccjlog = logging.getLogger("ccj")
del ccjlog.handlers[:]  # avoid issues with stubborn handlers
ccjlog.setLevel(10)
ccjlog.critical("beware that \"print\" is often delayed, while loggin isn't")

def find_file(directory, extension="in"):
    """ Find input file in a directory.
    
    Returns
    -------
     : str or int
        Path to file or 0 in case of AssertionError.
    """
    dir_list = [fn for fn in glob.glob(directory+"/*."+extension)]
    try:
        assert len(dir_list) == 1
        return dir_list[0]
    except AssertionError:
#        print("AssertionError: Could not determine single "+ extension +
#              "file in " + directory + " !")
        ccjlog.critical("AssertionError: Could not determine single "+ extension +
              "file in " + directory + " !")
        return 0
    
def find_output(directory, extension="out"):
    """ Find output file in a directory.
    
    Returns
    -------
     : str or int
        Path to output file or 0 in case of AssertionError.
    """
    dir_list = [fn for fn in glob.glob(directory+"/*."+extension)
                if not os.path.basename(fn).startswith("slurm")]
    try:
        assert len(dir_list) == 1
        return dir_list[0]
    except AssertionError:
#        print("AssertionError: Could not determine single .out \
#              file in " + directory + " !")
        ccjlog.critical("AssertionError: Could not determine single .out \
              file in " + directory + " !")
        return 0
    
def find_eleconfig(directory):
    """Find a suitable electronic configuration file in a directory.
    
    Returns
    ----------
     : str or int
         Path to 'eleconfig.txt' or 0 in case of AssertionError.
    """
    usual_suspects = ["eleconfiguration.txt", "eleconfig.txt",
                      "elconfig.txt", "eleconf.txt", "econf.txt",
                      "elconf.txt", "ele.config", "electronic.conf", 
                      "ccjob_elconfig.txt"
                     ]
    cand = [os.path.basename(fn) for fn in glob.glob(directory+"/*.txt")
            if os.path.isfile(fn)]
    cand.extend([os.path.basename(cf) for cf in glob.glob(directory+"/*.config")
                 if os.path.isfile(cf)])
    
    #intersec = usual_suspects & set(cand)
    intersec = [x for x in cand if x.lower() in usual_suspects]
    try:
        assert len(intersec) == 1
        return os.path.join(directory, intersec[0])
    except AssertionError:
#        print("-- No or more than one elec. config file detected.")
        ccjlog.critical("-- No or more than one elec. config file detected.")
#        print(("AssertionError: len(Candidate list) > 1 !\n"
#               "Please remove obsolete configuration files or "
#               "try saving the electronic configuration file using one of the"
#               "following file names:\n  - 'eleconf.txt'\n  - 'eleconfig.txt'"
#               "\n  - 'ele.config'"))
        return 0

class Input(object):
    def __init__(self, fname, inp_string=None, copy_input=False):
        self.input_string = inp_string
        self.filename = fname
        if not copy_input:
            self.save_input()
    
    @classmethod
    def from_template(cls, template, fname, **kwargs):
        """ Alternative constructor using string.Template
        
        Parameters
        ----------
        template : string.Template
            Template of input file
        **kwargs : key-value pairs
            keyworded arguments which have to match template
        """
        
        try:
            inp = template.substitute(defaults, **kwargs)
            return cls(fname, inp_string=inp)
        except (KeyError, ValueError) as error:
#            print(error)
#            print(error.args)
            ccjlog.critical("{}".format(error))
            ccjlog.critical("{}".format(error.args))
            quit(0)
            
    @classmethod
    def from_file(cls, path_src, path_new=None):
        """ Alternative constructor just copying an existing input.
        """
        cp_cond = [os.path.exists(path_src), os.path.isfile(path_src),
                   len(path_new) != 0]
        if path_new != None and all(cp_cond):
            sh.copy(path_src, path_new)
            if os.path.isdir(path_new):
                path_new = os.path.join(path_new, os.path.basename(
                        os.path.normpath(path_src)))
            return cls(path_new, inp_string=None, copy_input=True)
        else:
            return cls(path_src, inp_string=None, copy_input=False)
            
    def save_input(self):
        with open(self.filename, "w") as f:
            f.write(self.input_string)
#        print("-- Input file [", self.filename, "] written successfully.")
        ccjlog.info("-- Input file [{}] written successfully.".format(self.filename))

class Job(object):
    """ Job Constructor.
    
        Holds job information.
             
        Parameters
        ----------
        ccinput : CCJob.Input
            CCJob.Input instance.
        mem : int
            Memory in MB.
        cpus : int
            Number of CPUs.
        time : string
            Time in the format that SLURM accepts (dd-hh, hh:mm:ss).
        partition : string
            Partition name.
            
        Returns
        -------
        CCJob
            CCJob instance.
        """
        
    slurm_template = {"partition": "--partition=$partition",
                      "memory": "--mem=$memory",
                      "time": "--time=$time",
                      "cpus": "--cpus-per-task=$cpus",
                      "jobname": "--job-name=$jobname"
                      }
    def __init__(self, ccinput, script=None, queue="slurm", mem=500,
                 cpus=1, time="00:15:00", partition=None, jobname="CCJob",
                 software="qchem"):

        self.ccinput = ccinput
        self.script = script
        self.software = software
        self.queue = queue
        
        # TODO:
        # in future maybe use timedelta object instead of timestring
        # ==> more control over parameters
        #self.time = timedelta(days=t_days, hours=t_hours, minutes=t_mins)

#        self.basis = None
#        self._memory = None
        # submit options
        self.options = {"memory": mem, "cpus": cpus, "time": time,
                        "partition": partition, "jobname": jobname}
        
        self.jobid = None
#        self.scheduler = JobScheduler(qtype=queue)
        self.custom_options = []
        self.software_options = []
#    @property
#    def memory(self):
#        return self._memory
#    
#    @memory.setter
#    def memory(self, mem):
#        if mem <= 0:
#            raise ValueError("Negative or 0 memory detected!")
#        elif self.mem_unit.lower() == "gb":
#            self._memory = mem*1000
#        else:
#            self._memory = mem
    def get_job_options(self):
        """Prepare the string that holds all options for the queuing manager
        
        Returns
        -------
        argument : list
            Option string for queuing manager.
        """
        if self.queue.lower() == "slurm":
            argument = [string.Template(Job.slurm_template[key]).substitute(
                    {key : value}) for key, value in self.options.items()]
        else:
            NotImplementedError("Currently the script only supports SLURM!")
            
        if len(self.custom_options) > 0:
            argument += self.custom_options
            
        return argument
    
    def set_custom_options(self, *args, use_long=True, silent=True, **kwargs):
        """Adds custom queueing manager options.
        
        Parameters
        ----------
        use_long : bool
            Whether to use long option names which are usually prepended with
            "`--`". The key and value pair is then separated by "=".
            (Default: ``True``)
        silent : bool
            Whether to print the custom options to screen.
        
        Other Parameters
        ----------------
        args   : tuple
            Tuple of additional flags (no key-value-pair).
        kwargs : dict
            Keywords and their values.
            
        """
        # keyworded arguments
        if use_long:
            self.custom_options.extend(["{0}={1}".format(k, v) for k, v in
                                        kwargs.items()])
        else:
            self.custom_options.extend(["{0} {1}".format(k, v) for k, v in
                                        kwargs.items()])
        # flags
        self.custom_options.extend(args)
        
        if not silent:
#            print("-- Custom options specified: ", " ".join(self.custom_options))
            ccjlog.info("-- Custom options specified: {}".format(" ".join(self.custom_options)))
    
    def set_software_options(self, output=None, output_extension="out",
                             prepend="-", *args, **kwargs):
        """Set options for software that should be run interactively.
        
        .. deprecated:: 1.0.0
            `set_software_options()` will be removed in CCJob version 1.1.0
        """
        cmd = []
        # let's add options first
        if kwargs is not None:
            cmd.extend([prepend+"{0} {1}".format(k, val) for k, val in
                        kwargs.items()])
        # add input filename     
        cmd.append(self.ccinput.filename)
        # add output filename
        if output is None:
            cmd.append(os.path.splitext(self.ccinput.filename)[0]+"."+
                       output_extension)
        else:
            cmd.append(output)
        # add anything else if needed
        if args is not None:
            cmd.append()
        self.software_options = cmd

    
    def submit(self, dry_run=False, silent=False):
        """Submit job to queuing manager in batch mode. """
        q_arg = self.get_job_options()
        
        args = ["sbatch"] + q_arg + [self.script, self.ccinput.filename]
        arg_str = " ".join(args)
        if dry_run:
#            print("-- dry-run: ", arg_str)
            ccjlog.info("-- dry-run: {}".format(arg_str))
        else:
            if not silent:
#                print("-- running: ", arg_str)
                ccjlog.critical("-- running: {}".format(arg_str))
            p = sp.run(arg_str, stdout=sp.PIPE, stderr=sp.PIPE, shell=True)
            if len(p.stderr) > 0:
#                print("-- stderr: ", p.stderr)
                ccjlog.critical("-- stderr: {}".format(p.stderr))
            try:
                self.jobid = int(p.stdout.split()[-1])
            except IndexError:
#                print("-- stdout: ", p.stdout)
                ccjlog.critical("-- stdout: {}".format(p.stdout))
            
    def run(self, dry_run=False, silent=False, output=False):
        """Submit job to queuing manager in live mode. 
        
        This function uses the following commmand line structure:
        
        ``srun <SLURM options> script <input>``
        """
        q_arg = self.get_job_options()
        if not output:
            basename = ".".join(self.ccinput.filename.split(".")[:-1])
            output = "{}.out".format(basename)
        args = ["srun"] + q_arg + [self.software, self.ccinput.filename, output]
        arg_str = " ".join(args)
        if dry_run:
#            print("-- dry-run: ", arg_str)
            ccjlog.critical("-- dry-run: {}".format(arg_str))
        else:
            if not silent:
#                print("-- running: ", arg_str)
                ccjlog.critical("-- running: {}".format(arg_str))
            stdout = open("stdout.txt", "w")
            stderr = open("stderr.txt", "w")
            
            p = sp.run(arg_str, stdout=stdout, stderr=stderr, shell=True)
            
            stderr.close()
            stdout.close()

    def run_custom(self, silent=False):
        """Submit job to queuing manager in live mode. 
        
        Warning
        -------
        Since this command is started from the login node,
        environment variables on the compute node are **not set** which might
        cause the software to crash or not to start at all. Therefore this
        function is really only useful for software that is automatically
        available on the compute node (i.e. without changing $PATH).
        
        
        This function uses the following commmand line structure:
        
        ``srun <SLURM options> my_software <input> <output> <other options>``
        
        .. deprecated:: 1.0.0
            `run_custom()` will be removed in CCJob version 1.1.0
        """
        q_arg = self.get_job_options()

        args = ["srun"] + q_arg + [self.software] + self.software_options
        arg_str = " ".join(args)
#        print(arg_str)
        ccjlog.critical(arg_str)
        stdout = open("stdout.txt", "w")
        stderr = open("stderr.txt", "w")
        
        p = sp.run(arg_str, stdout=stdout, stderr=stderr, shell=True)
        
        stderr.close()
        stdout.close()
        if not silent:
#            print("Calculation finished.")
            ccjlog.critical("Calculation finished.")
        
###############################################################################
#   USEFUL TOOLS
###############################################################################
def zr_frag(fname, separator="----", fmt="string"):
    """ Get fragment coordinates from .zr file 
    
    Parameters
    ----------
    fname: string
        ZR filename
    separator: string
        Separator.
    fmt: string
        Format specifier. Possible options: "list", "string"
        
    Returns
    -------
    frags: dict
        Dictionary of list of lists with fragment coordinates and atom symbol,
        e.g. ``frags["A"] = ["C 0.0 0.1 0.0", "..."]``. If no separator is
        found (normal xyz file), only one fragment is assumed ("AB").
        
        If fragments are found ``frags`` will contain the keys 'A', 'B', 'AB',
        'AB_ghost'.
    
    
    """
    if fmt.lower() not in ("string", "list"):
        raise ValueError("Invalid format option specified! Use either 'string' or 'list'.")
    with open(fname) as zr:
        rl = zr.readlines()
    line_B = 0
    frags = {}
    for i, line in enumerate(rl):
        if separator in line:
            line_B = i
    
    if line_B == 0:
        frags["AB"] = list(map(str.split, rl[0:]))
    else:
        frags["A"] = list(map(str.split, rl[0:line_B]))
        frags["B"] = list(map(str.split, rl[line_B+1:]))
        frags["AB_ghost"] = frags["A"] + list(map(lambda x: ["@"+x[0]]+x[1:],
                                                  frags["B"]))
        frags["BA_ghost"] = list(map(lambda x: ["@"+x[0]]+x[1:], frags["A"]))+\
                            frags["B"]
        frags["AB"] = frags["A"] + frags["B"]

    if fmt=="list":
        return frags
    elif fmt=="string":
        for key in frags:
#            frags[key] = "\n".join(["\t".join(s) for s in frags[key]])
#            "{0:<8}{1:<16}{2:<16}{3:<16}".format(*s)
            frags[key] = "\n".join(["    ".join(s) for s in frags[key]])
        return frags



def read_eleconfig(fname="eleconfig.txt", silent=False):
    """Read electronic configuration from file.
    
    Parameters
    ----------
    fname : str
        Input file name (default: 'eleconfig.txt').
    silent : bool
        Whether to print information to screen or not.
    
    Returns
    -------
    eleconfig : dict
        Dictionary containing the electronic configuration. If no file was
        found, an empty dictionary will be returned instead.
    """
    import re
    p_chg = r"charge_(?P<frag>[A-Za-z0-9]+)\s*=\s*(?P<value>[-+]?\d+)"
    p_mul = r"multiplicity_(?P<frag>[A-Za-z0-9]+)\s*=\s*(?P<value>\d+)"
    eleconfig = {}
    try:
        if fname == 0:
            raise FileNotFoundError
        with open(fname) as el:
            for x in el:
                m = re.search(p_chg, x)
                if m:
                    if m.group("frag") == "tot":
                        eleconfig["charge_tot"] = int(m.group("value"))
                    elif m.group("frag") in ["A", "a", "1"]:
                        eleconfig["charge_a"]   = int(m.group("value"))
                    elif m.group("frag") in ["B", "b", "2"]:
                        eleconfig["charge_b"]   = int(m.group("value"))   
                m = re.search(p_mul, x)
                if m:
                    if m.group("frag") == "tot":
                        eleconfig["multiplicity_tot"] = int(m.group("value"))
                    elif m.group("frag") in ["A", "a", "1"]:
                        eleconfig["multiplicity_a"]   = int(m.group("value"))
                    elif m.group("frag") in ["B", "b", "2"]:
                        eleconfig["multiplicity_b"]   = int(m.group("value"))
#        print("-- Obtained electronic configuration from file '" +fname+ "'.")
        ccjlog.critical("-- Obtained electronic configuration from file '" +fname+ "'.")
    except FileNotFoundError:
#        print(("Could not find electronic configuration file. Using default "
#               "values for charge and multiplicity (c=0, m=1)."))
        ccjlog.critical(("Could not find electronic configuration file. Using default "
               "values for charge and multiplicity (c=0, m=1)."))
    return eleconfig
    
def electronicConf(*dct, fname="eleconfig.txt"):
    """Update dictionary with charge and multiplicity from file.


    """
    eleconfig = read_eleconfig(fname)
    if len(dct) == 1:
        dct[0].update(eleconfig)
    else:
        return eleconfig
