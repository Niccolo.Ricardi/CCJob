#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 11:17:36 2021

@author: nico
"""

import sys
from string import Template


stitch = Template("""$$comment
$comment
$$end
$$rem
sym_ignore = true
method = $method
basis = $basis
mem_static = 1024
fde = true
mem_total = $memory
molden_format = $molden
print_orbitals = $print_orbitals
$$end
$$molecule
$charge $multiplicity
$xyz
$$end
$$fde
expansion ME
rhoB_method HF
Superposition_B true
Import_Superposition_B true
T_Func $t_func
XC_Func $xc_func
$$end""")

Tinp = Template("""$$comment
$comment
$$end

$$rem
$rem_kw
$$end

$$molecule
$molecule
$$end

$extras
""")

Trem_kw = Template("""sym_ignore = true
method = $method
basis = $basis
fde = $fde
scf_convergence = $scf_conv
gen_scfman = false
mem_static = $mem_static
mem_total = $memory
molden_format = $molden
print_orbitals = $print_orbitals
fde_details = $fde_details
scf_print = $scf_print
scf_final_print = $scf_final_print
fde_export_density = $export
$ee_specs
$rem_extras""")

Tmolecule = Template("""$charge $multiplicity
$xyz""")

Tfragments = Template("""$charge_tot $multiplicity_tot
--
$charge_a $multiplicity_a
$frag_a
--
$charge_b $multiplicity_b
$frag_b""")

Tadc = Template("""ee_states = $nstates
adc_davidson_maxiter = 900
adc_davidson_conv = 6
adc_print = 3
adc_prop_es = true
state_analysis = true""")

Ttddft = Template("""$cis_n_roots = $cis_roots
cis_singlets = $singlets
cis_triplets = $triplets
rpa = $rpa
state_analysis = true""")

Tfde = Template("""$$fde
T_Func $t_func
$xc_func
expansion $expansion
$method_a
$method_b
PrintLevel $print_level
debug $debug
$fde_extras
$$end""")

# More specific Templates
Tpc = Template("""$$external_charges
$point_charges
$$end""")

Tchelpg_qchem = Template("""chelpg = true
chelpg_head = 30
chelpg_dx = 6""")

Talmo2 = Template("""jobtype = eda
eda2 = 1 !nDQ
$method_specs
eda_bsse = true
n_frozen_core = 0
""")

Talmo2_mp2 = Template("""exchange = hf
correlation = rimp2
aux_basis = $aux_basis
frgm_method = stoll""")

Tbasis = Template("""$$basis
$basis_specs
$$end""")

Tinps = Template("""$inp1

@@@

$inp2""")


# Defaults
A = ("C        -3.6180905689    1.3768035675   -0.0207958979\n"
     "O        -4.7356838533    1.5255563000    0.1150239130")
B = ("O        -7.9563726699    1.4854060709    0.1167920007\n"
     "H        -6.9923165534    1.4211335985    0.1774706091\n"
     "H        -8.1058463545    2.4422204631    0.1115993752")

pc = ("-7.9563726699    1.4854060709    0.1167920007    -0.8\n"
      "-6.9923165534    1.4211335985    0.1774706091     0.4\n"
      "-8.1058463545    2.4422204631    0.1115993752     0.4")

chelpg_kw = """chelpg = true
chelpg_head = 30
chelpg_dx = 6"""
rem_kw_defaults = {"basis"              : "cc-pVDZ",
                  "method"           : "HF",
                  "molden"           : "false",
                  "scf_conv"         : 7,
                  "scf_print"        : 0,
                  "scf_final_print"  : 1,
                  "rem_extras"       : "",
                  "fde"              : "false",
                  "mem_static"       : 1024,
                  "memory"           : 15000,
                  "print_orbitals"   : 15,
                  "export"           : "true",
                  "fde_details"      : 4,
                  "nstates"          : 0,
                  "ee_specs"         : ""
                  }

molecule_defaults = {"charge"           : 0,
                     "xyz"              : A,
                     "multiplicity"     : 1,
                     "charge_tot"       : 0,
                     "charge_a"         : 0,
                     "charge_b"         : 0,
                     "multiplicity_tot" : 1,
                     "multiplicity_a"   : 1,
                     "multiplicity_b"   : 1,
                     "frag_a"           : A,
                     "frag_b"           : B,
                     "point_charges"    : pc,
                   }

fde_defaults = {"expansion"        : "ME",
                "method_a"         : "rhoB_method HF",
                "method_b"         : "rhoA_method HF",
                "t_func"           : "TF",
                "xc_func"          : "x_func Slater\nc_func VWN5",
                "debug"            : "true",
                "print_level"      : 3,
                "fde_extras"       : "",
               }
tddft_defaults = {"cis_roots"        : "1",
                  "singlets"         : "true",
                  "triplets"         : "false",
                  "rpa"              : "false"
                  }
inp_defaults = {"comment"          : "Generated from CCJob.qchem_templates",
                "extras"           : ""}

adc_defaults = {"nstates"        : 0}

Tdefaults = dict(rem_kw=rem_kw_defaults, molecule=molecule_defaults,
                 fde=fde_defaults, tddft=tddft_defaults, inp=inp_defaults,
                 adc=adc_defaults)
