#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Aug  2 17:52:42 2021

@author: nico
"""
import os
import json as js
import CCJob as ccj
import CCParser as ccp
import subprocess as sp
import logging
import numpy as np

def setupLogger(to_console=True, to_log=False, logname="CCJob.log", printlevel=20):  
    """
    Note
    ----
    Never add handlers directly, particularly within functions that get called several times, as this will result in handler multiplication.
    Use this function which prevents it and ensure 1 handler per type is present at most.
    
    Parameters
    ----------
    to_console: bool
        whether to print logging to the console
    to_log: bool
        whether to print logging to file
    logname: str
        the log filename. Default is CCJob.log
    printlevel: int
        logging level: 10,20,30,40,50. Accessible also as logging.DEBUG/INFO/WARNING/ERROR/CRITICAL (respectively)
        
    Does
    ----
    Sets up the logger avoiding handlers repetition
    """
    if "ccjlog" not in globals(): 
        global ccjlog
        ccjlog = logging.getLogger("ccj")
    else:
        ccjlog = globals()["ccjlog"]
    handlernames = [i._name for i in ccjlog.handlers]
    handlerdict = {}
    if False in [type(to_console) == bool, type(to_log) == bool]:
        ccjlog.critical("\"to_console\" and \"to_log\" should be boolean!!")
        if type(to_log) == str and "." in to_log and logname == "CCJob.log":
            ccjlog.critical("""It appears you might have given your desired logname as \"to_log\" instead of changing \"logname\".+
                            I am using  {} as logname and setting to_log to True""".format(to_log))
            to_log, logname = True, to_log
    if to_console:
        handlerdict["console"] = logging.StreamHandler()
    if to_log:
        handlerdict["file"] = logging.FileHandler(logname)
    if handlerdict:  # there is at least a handler
        for k,handler in handlerdict.items():
            if k not in handlernames:
                handler._name = k
                handler.setLevel(printlevel)
                if printlevel == 10:
                    handler.setFormatter(logging.Formatter('%(levelname)s - %(name)s.%(funcName)s:  %(message)s'))
                else:
                    handler.setFormatter(logging.Formatter('%(message)s'))
                ccjlog.addHandler(handler)
    else: # there is no handler
        ccjlog.setLevel(50)
 
# set up logger. NB avoid homonimity with other module's loggers (e.g. ccp)
ccjlog = logging.getLogger("ccj")

def is_square(integer):
    root = np.sqrt(integer)
    if int(root + 0.5) ** 2 == integer: 
        return True
    else:
        return False
    
def logchdir(logger, path, printlevel=20):
    """
    Note
    ----
    logs the changing of directory
    
    Parameters
    ----------
    logger: obj
        the logger
    path: str
        the path to go to
    printlevel: int
        10,20,30,40,50, also accessible as logging.DEBUG, etc...
        printlevel to pass to the logger
    """
    from_ = os.getcwd()      
    message = "{} ==> {}".format(from_, path)
    try:
        os.chdir(path)
    except FileNotFoundError:
        logger.critical("{} DOES NOT EXIST!!".format(path))
        raise FileNotFoundError
    if printlevel == 10:
        logger.debug(message)
    elif printlevel == 20:
        logger.info(message)
    elif printlevel == 30:
        logger.warning(message)
    elif printlevel == 40:
        logger.error(message)
    elif printlevel == 50:
        logger.critical(message)        
        
def mkdif(path):
    """
    Parameters
    ----------
    path: str
        the path to create if not present
    """
    if not os.path.isdir(path):
        os.mkdir(path)
        
def myupd(d,*args,**kwargs):
    """
    Parameters
    ----------
    d: dict
        the dictionary to update
    args: dict
        the dictionaries to update with
    kwargs: kwargs
        kwargs to update with
    Returns
    -------
    dict
       the updated dictionary
    """
    dcp = d.copy()
    for a in args:
        dcp.update(a)
    dcp.update(kwargs)
    return dcp

def read_file(fname):
    """
    Parameters
    ----------
    fname: str
        filepath to load
    
    Returns
    -------
    str
        the file's content
    """
    with open(fname, "r") as f:
        string = f.read()
    return string

def load_js(fname):
    """
    Note
    ----
    Some datatype may change when load=>dump=>load (e.g. tuple=> list, {1:"one"}=>{'1':'one'})
    
    Parameters
    ----------
    fname: str
        filepath to load
    
    Returns
    -------
    dict
        the dictionary in the json file
    """
    with open(fname,"r") as f:
        return js.load(f)

def dump_js(obj,fname):
    """
    Note
    ----
    Some datatype may change when load=>dump=>load (e.g. tuple=> list, {1:"one"}=>{'1':'one'})
    
    Parameters
    ----------
    obj: dict
        the dictionary to dump
    fname: str
        filepath to dump in
    
    Does
    ----
    dumps obj in fname
    """
    with open(fname,"w") as f:
        js.dump(obj,f)

class FailedCalculation(Exception):
    """May be raised if a calculation fails"""
    pass
        
def run_job(specs, queue, meta, template, q_custom=None, batch_mode=False):
    """ Prepare and run job then save status.
    debug_L

    Parameters<
    ----------
    specs : dict
        Dictionary containing embedding information (used for template).
    queue : dict
        Dictionary for queuing manager variables (see :class:`CCJob.Job`).
    meta : dict
        Dictionary containing the calculation's meta information.
    template : :obj:`string.Template`
        Calculation template
    q_custom : dict
        Custom options for queuing manager (see
        :func:`CCJob.Job.set_custom_options`)
    batch_mode : bool
        Whether to run the job in interactive or batch mode.
    """
    cwd = os.getcwd()
    # throw error if there's no path
    if "path" not in meta.keys():
        raise KeyError("Calculation path not defined!")

    # create folder if necessary
    mkdif(meta["path"])
    logchdir(ccjlog,meta["path"])

    # Prepare input
    inputFile = meta["basename"] + ".in"
    inp = ccj.Input.from_template(template, inputFile, **specs)

    # Prepare job
    queue["jobname"] = os.path.basename(meta["path"])
    job = ccj.Job(inp, **queue)
    if q_custom is not None:
        job.set_custom_options(**q_custom)

    # Run or submit the job
    if batch_mode:
        job.submit()
        meta["status"] = "PENDING"
        meta["jobid"] = job.jobid
    else:
        job.run(output="{}.out".format(meta["basename"]))
        meta["jobid"] = job.jobid

        # Parse output
        out = ccp.Parser(meta["basename"]+".out", software="qchem",
                         to_file=True, to_console=False, to_json=True)

        # did the calculation finish correctly?
        if out.results.has_finished[-1]:
            meta["status"] = "FIN"
        else:
            meta["status"] = "FAIL"
    logchdir(ccjlog,cwd)

def save_status(meta, meta_file="meta.json"):
    """Dump meta information in json format.

    Parameters
    ----------
    meta : dict
        Dictionary holding the meta information. Must include "path" as an abspath
    meta_file : str
        File name of meta file (default: 'meta.json'). NOT FILEPATH!
    """
    meta_file = os.path.join(meta["path"], meta_file)
    dump_js(meta, meta_file)

def get_queue(*args):
    """
    Note
    ----
    Options can be used to make it faster.
    NB. use --options=value as one arg or "-o","value" as 2 args
    e.g. "--user=ricardi" or "-u","ricardi"
    
    Parameters
    ----------
    args: str
        optional arguments for squeue
    """
    command = ["squeue"]
    if len(args) > 0:
        for arg in args:
            command.append(arg)
    return str(sp.check_output(command))
    
def isinq(jobid, *args, queue_str=""):
    """
    Note
    ----
    checks the queue and searches "jobid".
    Options can be used to make it faster.
    NB. use --options=value as one arg or "-o","value" as 2 args
    e.g. "--user=ricardi" or "-u","ricardi"
    
    Parameters
    ----------
    jobid: int/str
        the jobID
    args: str
        optional arguments for squeue
    queue_str: str
        output of squeue. if empty, will be obtained
    
    Returns
    -------
    bool
        whether JobID is in queue
    """
    if jobid is None:  # "None" may be in the output of squeue. 
        #We want to assure that isinq(None)==False. None happens when no job is submitted (partition errors, etc...)
        return False
    if not queue_str:
        queue_str = get_queue(*args)
    to_return = "{}".format(jobid) in queue_str
    return to_return

def status_ok(*args, meta_file="meta.json", path=None, software="qchem",
              break4fail=False, printout=True, queue_str=""):
    """
    Note
    ----
    Check if a calculation exists and has finished correctly and
    updates meta file.

    Parameters
    ----------
    meta_file : str
        File name of meta file (default: 'meta.json').
    path : str
        Directory in which meta file should be located (default: current
        working directory). Please note the default assumes that one is already
        located in the folder that contains the meta file.
    args: str
        optional arguments for squeue
    queue_str: str
        output of squeue. if empty, will be obtained
    software: str
        software keyword for CCParser
    printout: bool
        whether to printout if the job is in the queue
    break4fail: bool
        if False a failed calculation will return False, if True it will raise an error
        
    Returns
    -------
    bool or None
            True for successful calculation, False for somehow failed, None for pending
    """
    if path is None:
        path = os.getcwd()
    path_to_meta = os.path.join(path, meta_file)
    if os.path.exists(path_to_meta):
        from_file = load_js(path_to_meta)
        if "basename" in from_file.keys():
            path_to_out = os.path.join(path, from_file["basename"]+".out")
            path_to_log = os.path.join(path, "CCParser.log")
            path_to_json = os.path.join(path, "CCParser.json")
        try:
            if from_file["status"] == "PENDING":
                if isinq(from_file["jobid"], ):
                    if printout:
                        ccjlog.info("job {} is in queue".format(from_file["jobid"]))
#                        print("job {} is in queue".format(from_file["jobid"]))
                    return None
                elif os.path.isfile(path_to_out):
                    out = ccp.Parser(path_to_out, software=software,
                                     to_file=True, to_console=False,
                                     to_json=True, log_file=path_to_log,
                                     json_file=path_to_json)
                    if out.results.has_finished[-1]:
                        from_file["status"] = "FIN"
                        to_return = True
                    else:
                        from_file["status"] = "FAIL"
                        to_return = False
                    dump_js(from_file,path_to_meta)
                    return to_return
                else:
                    return False
            elif from_file["status"] == "FIN":
                return True
            elif from_file["status"] == "FAIL":
                if break4fail:
                    raise FailedCalculation(path_to_out+" has failed."
                                            " Check it!")
                else:
                    return False
        except KeyError:
            return False
    else:
        return False

def get_last_iter_dir(active="A", dirbase="cy", path=os.getcwd(),opt="fnt"):
    """Find the last folder of Freeze-and-Thaw or Macrocycle iterations.
    
    Parameters
    ----------
    active : :obj:`str`
        Which system is considered active (default: 'A').
    dirbase : :obj:`str`
        Basename for iteration folders without numbering (default: 'cy').
    path : :obj:`str`
        Directory in which iteration folders are located (default: $PWD).
    """
    WF_opt = {"macrocycles" : "MC", "macro" : "MC", "freeze-thaw" : "FT",
                           "freeze-and-thaw" : "FT", "fnt" : "FT", None : "EMB"}
    s_idx = len(dirbase)
    cy = [d for d in os.listdir(path) if os.path.isdir(os.path.join(path, d)) \
          and d.startswith(dirbase)]
    iterations = [int(s[s_idx:]) for s in cy]
    final_iter = max(iterations)
    if WF_opt[opt]=="FT":
        iseven = final_iter % 2 == 0
        if (active == "A" and iseven) or (active == "B" and not iseven):
            return os.path.join(path, dirbase + str(final_iter))
        elif (active == "A" and not iseven) or (active == "B" and iseven):
            return os.path.join(path, dirbase + str(final_iter-1))
        else:
            raise ValueError("Could not determine last iteration folder!")
    elif WF_opt[opt]=="MC":
        return os.path.join(path, dirbase + str(final_iter))
    else:
        raise ValueError("Could not determine last iteration folder!")